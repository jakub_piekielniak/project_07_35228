Instrukcja dla użytkownika
1.Na sam początek na konsoli pojawi się ekran powitalny. Do gry przechodzimy automatycznie.
2.Na ekranie będzie się nam wyświetlać:
- pytanie, 
- odpowiedzi, 
- wszystkie dostępne koła ratunkowe oraz opcja rezygnacji
3.Z każdego koła ratunkowego można skorzystać tylko raz. 
4.Do jednego pytania można użyć więcej niż jedno koło ratunkowe.
5.W celu udzielenia odpowiedzi należy wpisać literę (nie ważne czy małą czy dużą) oraz nacisnąć enter.
6.Z gry można zrezygnować w każdym momencie, ale gwarantowana wygrana jest tylko z etapu 4 i 9.
7.Aby wygrać milion należy odpowiedzieć na wszystkie 15 pytań.


Narzędzia użyte w celu stworzenia tworzenia projektu
1.Qt Ceator/CodeBlocks
2.Notepad/Excel/WPS Office
3.Bitbucket
4.MakeReadMe.com


Podział pracy
Jakub Piekielniak:
- Utworzenie repozytorium
- Pytania 1-5
- Klasy Question i Wallet
- Koło ratunkowe „50/50”
- Komendy dodające kolor do konsoli
- Funkcja wczytująca odpowiedź
- Funkcja sterująca grą
- Push projektu na mastera

Justyna Kurcz:
- Zmienne globalne
- Funkcja wczytująca
- Pytania 6-10
- Funkcja Help
- Koło ratunkowe „Pytanie do publiczności”
- Wrzucenie pytań na Bitbucket

Izabela Panek:
- Funkcja wyświetlająca pytania
- Funkcja „ekran powitalny”
- Pytania 11-15
- Koło ratunkowe „Telefon do przyjaciela”
- Komendy wstrzymujące ekran
- Plik Readme


Autorzy
Jakub Piekielniak
Justyna Kurcz
Izabela Panek
