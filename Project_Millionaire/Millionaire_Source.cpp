#include "Millionaire_Header.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <time.h>
#include <math.h>
#include <windows.h>
#include <thread>

using namespace std;

bool  Phone_Friend = true, Question_To_Public = true, Fifty_Fifty = true;
int level = 1;
bool Game_Status = true;
int tmpL;
char tmpQ;
char first;
char Correct_Answer;


HANDLE hOut;


int Read_File2(string &quest, string &Answer_A, string &Answer_B, string &Answer_C, string &Answer_D, char &Correct_Answer, int &number_Quest)
{
    quest.clear();
    Answer_A.clear();
    Answer_B.clear();
    Answer_C.clear();
    Answer_D.clear();

    srand(time(NULL));
    fstream file;
    string tmp;
    int x=0;
    string name_file = "../../project_07_35228/Question/"+ to_string(level) + ".csv";
    file.open(name_file, ios::in);

    if(!file.good())
    {
        cout << "Nie mozna otworzyc pliku" << endl;
        file.close();
        return 1;
    }

    number_Quest = rand() % 10 + 1;

    for(int i = 0; i < number_Quest - 1; i++) getline(file, tmp);
    getline(file, tmp);
    Correct_Answer = tmp[tmp.length()];
    for(unsigned i = 0; i < tmp.length(); i++)
    {
        if (tmp[i] == ';')
        {
            tmp[i] = ' ';
            x++;
        }
        switch(x)
        {
        case 0: quest += tmp[i]; break;
        case 1: Answer_A += tmp[i]; break;
        case 2: Answer_B += tmp[i]; break;
        case 3: Answer_C += tmp[i]; break;
        case 4: Answer_D += tmp[i]; break;
        case 5: Correct_Answer += tmp[i]; break;
        }
    }
    tmpQ=Correct_Answer;
    file.close();
    return 0;
}

void Wallet::Resignation(){
    hOut = GetStdHandle( STD_OUTPUT_HANDLE );
        SetConsoleTextAttribute(hOut, 0xA);
    cout<<"Twoja wygrana : " << value << " zl " <<  endl;
    cout<<"Gratulacje!"<<endl;
    SetConsoleTextAttribute(hOut, 0x07);
}

void Wallet::GameLose(){
    hOut = GetStdHandle( STD_OUTPUT_HANDLE );
    SetConsoleTextAttribute(hOut, 0xA);
    cout<<"Bezpieczna wygrana : "<< saveSum << " zl " << endl;
    SetConsoleTextAttribute(hOut, 0x07);
}


void Wallet::Show_Wallet(){
    cout << "Aktualna wygrana : " << value << " zl " << endl;
    cout << "Pytanie za " << next << " zl " << endl;
    cout << "Kwota gwarantowana " << saveSum << endl;
}

void Wallet::changeWallet( int level ){

    switch( level ){
    case 1 :
        value = 0;
        next = Wallet_price[ level-1 ];
        saveSum = 0;
        break;

    case 2 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = 0;
        break;

    case 3 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = 0;
        break;

    case 4 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = 0;
        break;

    case 5 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = 0;
        break;

    case 6 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = Wallet_price[ 4 ];
        break;

    case 7 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = Wallet_price[ 4 ];
        break;

    case 8 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = Wallet_price[ 4 ];
        break;

    case 9 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = Wallet_price[ 4 ];
        break;

    case 10 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = Wallet_price[ 4 ];
        break;

    case 11 :
        value = Wallet_price[ level -2 ];
        next = Wallet_price[ level-1 ];
        saveSum = Wallet_price[ 9 ];
        break;

    case 12 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = Wallet_price[ 9 ];
        break;

    case 13 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = Wallet_price[ 9 ];
        break;

    case 14 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = Wallet_price[ 9 ];
        break;

    case 15 :
        value = Wallet_price[ level-2 ];
        next = Wallet_price[ level-1 ];
        saveSum = Wallet_price[ 9 ];
        break;

    default : Wallet(); break;
    }
}

void Question::Show_Question(){
    cout << Query << endl;
    cout << "A)" << Answer_A << endl;
    cout << "B)" << Answer_B << endl;
    cout << "C)" << Answer_C << endl;
    cout << "D)" << Answer_D << endl;

    cout << endl;
    this_thread::sleep_for(1500ms);
    Help();

}

char Sign(){
    char sign = ' ';
    cout << "Podaj odpowiedz lub skorzystaj z kola ratunkowego : ";
    cin>>sign;
    return sign;

}

bool Answer_User(Question  pytanie1, int& level, Wallet Game){
    char answer = Sign();

    if(answer == 'a') answer = 'A';
    if(answer == 'b') answer = 'B';
    if(answer == 'c') answer = 'C';
    if(answer == 'd') answer = 'D';

    if(answer == '4') {
        this_thread::sleep_for(500ms);
        system("cls");
        Game.Resignation();
        return 0;
    }
    if(answer == '3' && Fifty_Fifty==true)
    {
        fifty();
        return Answer_User(pytanie1, level, Game);
    }

    if(answer=='3'&&Fifty_Fifty==false)
    {
        cout<<"Juz zuzyles to kolo ratunkowe. Podaj odpowiedz lub skorzystaj z innego. "<<endl;
        return Answer_User(pytanie1, level, Game);
    }

    if(answer == '1' &&Phone_Friend==true)
    {
        PhoneToFriend();
        return Answer_User(pytanie1, level, Game);
    }

    if(answer=='1'&&Phone_Friend==false)
    {
        cout<<"Juz zuzyles to kolo ratunkowe. Podaj odpowiedz lub skorzystaj z innego. "<<endl;
        return Answer_User(pytanie1, level, Game);
    }

    if (answer=='2'&&Question_To_Public==true)
    {
        QuestionToPublic();
        return Answer_User(pytanie1, level, Game);
    }
    if (answer == '2'&&Question_To_Public == false)
    {
        cout<<"Juz zuzyles to kolo ratunkowe. Podaj odpowiedz lub skorzystaj z innego. "<<endl;
        return Answer_User(pytanie1, level, Game);
    }

    if(answer == 'A' || answer == 'B' || answer == 'C' || answer == 'D'){



        if(pytanie1.correct == answer){


            this_thread::sleep_for(1500ms);
            if(level == 15){
                hOut = GetStdHandle( STD_OUTPUT_HANDLE );
                SetConsoleTextAttribute(hOut, 0xA );

                cout<< " Gratulacje! Wygrales 1'000'000"<<endl;

                SetConsoleTextAttribute(hOut, 0x07 );
                return true;
            }




            if(level < 14 ) {
                hOut = GetStdHandle( STD_OUTPUT_HANDLE );
                SetConsoleTextAttribute(hOut, 0xA );
                cout<<"Poprawna odpowiedz, przechodzisz do kolejnego etapu!"<<endl;
                SetConsoleTextAttribute( hOut, 0x07 );
                this_thread::sleep_for(1500ms);
                system("cls");

                for(int i = 0 ; i < 100 ; i++ ) cout<<"+";
                cout<<endl;
                level++;
                return true;
            }



        }

        else {
            this_thread::sleep_for(1500ms);
            hOut = GetStdHandle( STD_OUTPUT_HANDLE );
            SetConsoleTextAttribute(hOut, 0xC);
            system("cls");
            cout<<"Bledna odpowiedz. Koniec gry!"<<endl;
            SetConsoleTextAttribute(hOut, 0x07 );
            Game.GameLose();
            return false;

        }


    }

    return Answer_User(pytanie1,level,Game);
    tmpL=level;

}

void Start_Game(){
    hOut = GetStdHandle( STD_OUTPUT_HANDLE );
    SetConsoleTextAttribute(hOut, 0x9);
    cout << setw(55) << "Witaj w Milionerzy!" << endl;;
    cout << setw(70) << "Gra sklada sie z 15 pytan z zakresu wiedzy ogolnej" << endl;;
    cout << setw(75) << "Po udzieleniu poprawnej odpowiedzi przejdziesz do kolejnej rundu" << endl;;
    cout << setw(95) << "Do wykorzystania masz 3 kola ratunkowe - 50/50, telefon do przyjaciela, pomoc publicznosci" << endl;;
    cout << setw(50) << "Powodzenia!" << endl;;
    SetConsoleTextAttribute(hOut, 0x07);
    system("pause");
    system("cls");

}

void Help(){

    if( Phone_Friend == true ) cout << "1. Telefon do przyjaciela" << endl;
    if( Question_To_Public == true ) cout << "2. Pytanie do publicznosci" << endl;
    if( Fifty_Fifty == true ) cout<< "3. 50/50" << endl;
    cout << "4. Rezygnacja " << endl;
}

void PhoneToFriend()
{
    char random, random2;
    char letters[4]={'a','b','c','d'};
    char letters2[2]={first, Correct_Answer};
    srand(time(0));

    if(tmpL<=5)
    {
        cout << "Moim zdaniem poprawna odpowiedzia jest : " << tmpQ << endl;
    }

    else if (Fifty_Fifty==true)
    {
        if(tmpL>5)
        {

            random2 = letters2[rand() % 2];
            cout << "Nie jestem pewien, ale mysle ze to odpowiedz: " << random2 << endl;
        }
    }
    else
    {
        random = letters[rand() % 4];
        cout << "Nie jestem pewien, ale mysle ze to odpowiedz: " << random << endl;
    }
    Phone_Friend=false;
}
void QuestionToPublic()
{

    srand( time(0) );
    int suma = 100;
    int correct = rand() % 51 + 50;
    suma -= correct;

    int a1=0, a2=0;
    int z1,z2,z3,z4;

    z1=rand()%suma;
    z2=100-(correct+z1);
    z3=rand()%z2;
    z4=100-(correct+z1+z3);

    if (Fifty_Fifty==false)
    {
        switch(tmpQ)
        {
        case 'a':
            if(first=='b')
            {
                cout<<"A)"<<correct<<"%"<<endl;
                cout<<"B)"<<suma<<"%"<<endl;
                cout<<"C)"<<a1<<"%"<<endl;
                cout<<"D)"<<a2<<"%"<<endl;
            }
            else if (first=='c')
            {
                cout<<"A)"<<correct<<"%"<<endl;
                cout<<"B)"<<z3<<"%"<<endl;
                cout<<"C)"<<suma<<"%"<<endl;
                cout<<"D)"<<z4<<"%"<<endl;
            }
            else if(first=='d')
            {
                cout<<"A)"<<correct<<"%"<<endl;
                cout<<"B)"<<a1<<"%"<<endl;
                cout<<"C)"<<a2<<"%"<<endl;
                cout<<"D)"<<suma<<"%"<<endl;
            }
            break;
        case 'b':
            if(first=='a')
            {
                cout<<"A)"<<suma<<"%"<<endl;
                cout<<"B)"<<correct<<"%"<<endl;
                cout<<"C)"<<a1<<"%"<<endl;
                cout<<"D)"<<a2<<"%"<<endl;
            }
            else if (first=='c')
            {
                cout<<"A)"<<a1<<"%"<<endl;
                cout<<"B)"<<correct<<"%"<<endl;
                cout<<"C)"<<suma<<"%"<<endl;
                cout<<"D)"<<a2<<"%"<<endl;
            }
            else if(first=='d')
            {
                cout<<"A)"<<a1<<"%"<<endl;
                cout<<"B)"<<correct<<"%"<<endl;
                cout<<"C)"<<a2<<"%"<<endl;
                cout<<"D)"<<suma<<"%"<<endl;
            }
            break;
        case 'c':
            if(first=='a')
            {
                cout<<"A)"<<suma<<"%"<<endl;
                cout<<"B)"<<a1<<"%"<<endl;
                cout<<"C)"<<correct<<"%"<<endl;
                cout<<"D)"<<a2<<"%"<<endl;
            }
            else if (first=='b')
            {
                cout<<"A)"<<a1<<"%"<<endl;
                cout<<"B)"<<suma<<"%"<<endl;
                cout<<"C)"<<correct<<"%"<<endl;
                cout<<"D)"<<a2<<"%"<<endl;
            }
            else if(first=='d')
            {
                cout<<"A)"<<a1<<"%"<<endl;
                cout<<"B)"<<correct<<"%"<<endl;
                cout<<"C)"<<a2<<"%"<<endl;
                cout<<"D)"<<suma<<"%"<<endl;
            }
            break;
        case 'd':
            if(first=='a')
            {
                cout<<"A)"<<suma<<"%"<<endl;
                cout<<"B)"<<a1<<"%"<<endl;
                cout<<"C)"<<a2<<"%"<<endl;
                cout<<"D)"<<correct<<"%"<<endl;
            }
            else if (first=='b')
            {
                cout<<"A)"<<a1<<"%"<<endl;
                cout<<"B)"<<suma<<"%"<<endl;
                cout<<"C)"<<a2<<"%"<<endl;
                cout<<"D)"<<correct<<"%"<<endl;
            }
            else if(first=='c')
            {
                cout<<"A)"<<a1<<"%"<<endl;
                cout<<"B)"<<a2<<"%"<<endl;
                cout<<"C)"<<suma<<"%"<<endl;
                cout<<"D)"<<correct<<"%"<<endl;
            }
            break;
        }
    }
    else
    {
        switch(tmpQ)
        {
        case 'a':
            cout<<"A)"<<correct<<"%"<<endl;
            cout<<"B)"<<z1<<"%"<<endl;
            cout<<"C)"<<z3<<"%"<<endl;
            cout<<"D)"<<z4<<"%"<<endl;
            break;
        case 'b':
            cout<<"A)"<<z1<<"%"<<endl;
            cout<<"B)"<<correct<<"%"<<endl;
            cout<<"C)"<<z3<<"%"<<endl;
            cout<<"D)"<<z4<<"%"<<endl;
            break;
        case 'c':
            cout<<"A)"<<z1<<"%"<<endl;
            cout<<"B)"<<z3<<"%"<<endl;
            cout<<"C)"<<correct<<"%"<<endl;
            cout<<"D)"<<z4<<"%"<<endl;
            break;
        case 'd':
            cout<<"A)"<<z1<<"%"<<endl;
            cout<<"B)"<<z3<<"%"<<endl;
            cout<<"C)"<<z4<<"%"<<endl;
            cout<<"D)"<<correct<<"%"<<endl;
            break;
        }
    }
    Question_To_Public=false;
}

void fifty()
{
    char letters[4]={'a','b','c','d'};
    srand(time(0));


    for( ; ; )
    {
        first = letters[rand() % 4];

        if( first == tmpQ )
        {
            continue;
        } else
        {
            break;
        }
    }
    cout << "Odpowiedzi, ktore pozostaly: " << first << " oraz " << tmpQ << endl;
    Fifty_Fifty=false;
}


