 #ifndef MILLIONAIRE_HEADER_H
#define MILLIONAIRE_HEADER_H

#include <string>
#include <iostream>
#include <windows.h>


int Read_File2(std::string &quest, std::string &Answer_A, std::string &Answer_B, std::string &Answer_C, std::string &Answer_D, char &Correct_Answer, int &number_Quest);
void Start_Game();
void Help();
char Sign();
bool Answer_User(class Question pytanie1, int& level, class Wallet Game);
void PhoneToFriend();
void QuestionToPublic();
void fifty();

using namespace std;

class Question{
public:

    char correct;
    string Query, Answer_A, Answer_B, Answer_C, Answer_D;
    void Show_Question();


    Question(){
        Query.clear();
        Answer_A.clear();
        Answer_B.clear();
        Answer_C.clear();
        Answer_D.clear();
    }

    Question( string quest, string A, string B, string C, string D, char correct_Answer ){
        Query = quest;
        Answer_A = A;
        Answer_B = B;
        Answer_C = C;
        Answer_D = D;
        correct = toupper(correct_Answer);
    }

    ~Question() {};




};

class Wallet{

public:

    int value, next, saveSum;
    void Show_Wallet();
    void changeWallet( int level );
    void GameLose();
    void Resignation();
    Wallet(){
        value = 0;
        next = 0;
        saveSum = 0;
    }

    Wallet( int _sum, int _nextSum, int _saveSum ){
        value = _sum;
        next = _nextSum;
        saveSum = _saveSum;
    }

    ~Wallet() {};
private:
    int Wallet_price[15] = { 100,200,300,500,1000,2000,4000,8000,16000,32000,64000,125000,250000,500000,1000000 };


};

#endif // MILLIONAIRE_HEADER_H
