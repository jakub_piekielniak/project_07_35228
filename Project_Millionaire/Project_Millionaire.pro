TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Millionaire_Source.cpp \
        main.cpp

HEADERS += \
    Millionaire_Header.h

DISTFILES += \
    README.md
